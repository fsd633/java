package day08;

public class Adamnum {

    public static boolean isAdamnum(int num) {
        int squareNum = num * num;
        int reverseOriginalNum = reverseDigits(num);
        int squareReverseNum = reverseOriginalNum * reverseOriginalNum;
        int reverseSquareNum = reverseDigits(squareNum);

        return squareReverseNum == reverseSquareNum;
    }

    public static int reverseDigits(int num) {
        int reversed = 0;
        for (int temp = num; temp != 0; temp /= 10) {
            int digit = temp % 10;
            reversed = reversed * 10 + digit;
        }
        return reversed;
    }

    public static void main(String[] args) {
        int num = 11;

        if (isAdamnum(num)) {
            System.out.println(num + " is an Adam Number");
        } else {
            System.out.println(num + " is not an Adam Number");
        }
    }
}
