package day08;

public class PerfectNumber {
	
	public static int isPerfect(int number){
		
		int sum = 0;
		
		for(int i = 1; i < number/2; i++){
			if(number % i == 0){
				sum =sum + i;
			}
		}
		return sum;
		
	} 

	public static void main(String[] args) {
		
		int number = 28;
		int s = isPerfect(number);
		
		if(s == number){
			System.out.println(number + "is a perfect number");
		}
		else{
			System.out.println(number + "is not a perect number");
		}
		

	}

}