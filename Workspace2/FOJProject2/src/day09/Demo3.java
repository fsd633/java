package day09;

public class Demo3 {

	public static boolean isIdentityMatrix(int arr[][]) {
		// TODO Auto-generated method stub
		
		//write the code here
		 for (int i = 0; i < 3; i++) {
	            for (int j = 0; j < 3; j++) {
	                if (i == j && arr[i][j] != 1|| (i != j && arr[i][j] != 0)){
	                	return false;
	            }}
		 }
		return true;

	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int arr1[][] = new int[][]{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
		int arr2[][] = new int[][]{{11, 22, 33}, {44, 55, 66}, {77, 88, 99}};
		int arr3[][] = new int[][]{{12, 23, 34}, {45, 56, 67}, {78, 89, 91}};
		int arr4[][] = new int[][]{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};

		System.out.println(isIdentityMatrix(arr1)); //true
		System.out.println(isIdentityMatrix(arr2)); //false
		System.out.println(isIdentityMatrix(arr3)); //false
		System.out.println(isIdentityMatrix(arr4)); //true
		
        }      
	
    }
