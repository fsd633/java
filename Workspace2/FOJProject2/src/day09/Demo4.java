package day09;

public class Demo4 {

	public static void findrowsum(int arr[][]){

		int sum = 0;

		for (int i = 0; i < 3; i++) 
		{ 
			for (int j = 0; j < 3; j++) 
			{ 

				sum=sum+arr[i][j];
				
			} 
			System.out.print("row-"+ (i+1) + "sum = "+ sum + " ");
		} 
		System.out.println();
	}


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int arr1[][] = new int[][]{{10, 20, 30}, {40, 50, 60}, {70, 80, 90}};
		int arr2[][] = new int[][]{{11, 22, 33}, {44, 55, 66}, {77, 88, 99}};
		int arr3[][] = new int[][]{{12, 23, 34}, {45, 56, 67}, {78, 89, 91}};

		findrowsum(arr1);
		findrowsum(arr2);
		findrowsum(arr3);
	}
}
