package day09;

public class Demo6 {

    public static int[][] matrixSum(int matrix1[][], int matrix2[][]) {
        int rows = matrix1.length;
        int cols = matrix1[0].length;
        int sum[][] = new int[rows][cols];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                sum[i][j] = matrix1[i][j] + matrix2[i][j];
            }
        }

        return sum;
    }

    public static void displayArray(int arr[][]) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int arr1[][] = new int[][]{{10, 20, 30}, {40, 51, 60}, {10, 20, 30}};
        int arr2[][] = new int[][]{{11, 12, 13}, {14, 15, 16}, {10, 20, 30}};

        int sum[][] = matrixSum(arr1, arr2);
        displayArray(arr1);
        displayArray(arr2);
        displayArray(sum);
    }
}
