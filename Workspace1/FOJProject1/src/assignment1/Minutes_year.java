package assignment1;

public class Minutes_year {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        int minutes = 1000000; // Example: Convert 1,000,000 minutes to years
        double years = convertMinutesToYears(minutes);
        System.out.println(minutes + " minutes is approximately " + years + " years.");
    }

    public static double convertMinutesToYears(int minutes) {
        // Number of minutes in a year considering leap years
        double minutesInYear = 365.25 * 24 * 60;
        
        // Calculate the years
        double years = minutes / minutesInYear;
        return years; 
    }
    
}
