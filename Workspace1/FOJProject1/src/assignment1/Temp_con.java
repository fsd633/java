package assignment1;

public class Temp_con {
	
	public static double CtoF(int C) {
        double F = (C * 1.8) + 32;
        return F;
    }

	public static double FtoC(int F) {
       double C = (F-32)*5/9;
       return C;
	}
	
	
	
	public static void main(String[] args) {
	
		double result=CtoF(27);
		double output=FtoC(27);
       System.out.println("Celsius to Fahrenheit "+result);
       System.out.println(" Fahrenheit to Celsius  "+output);
   	
	}

}
