package assignmet2;

public class ThreeDigitOfPalindrome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
			
			int num = 111;
			
			int firstDigit = num / 100;
			int lastDigit = num % 10;
			
			System.out.println((firstDigit == lastDigit) ? (num + " is a palindrome. ") : (num + " is not a palindrome."));

	}

}
