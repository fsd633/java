package assignmet2;

public class Divisible_3_5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
 //another program to check number is divisible by the 3 and 5
        
        int number = 15; // Change this to the number you want to check
        
        // Check if the number is divisible by 3 and 5
        boolean divisibleBy3 = number % 3 == 0;
        boolean divisibleBy5 = number % 5 == 0;
        
        // Print the result
        if (divisibleBy3 && divisibleBy5) {
            System.out.println("fizzbizz");
        } else if(divisibleBy5){
            System.out.println("bizz");
        }else {
        	System.out.println("fizz");
	}


	}

}
