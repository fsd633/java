package assignmet2;

public class SumOfDigits {

    public static void main(String[] args) {
        // Number to calculate the sum of digits
        int number = 347;

        // Calculate the sum of digits
        int sum = (number / 100) + ((number % 100) / 10) + (number % 10);

        // Print the sum of digits
        System.out.println("The sum of the digits in " + number + " is: " + sum);
    }

}
