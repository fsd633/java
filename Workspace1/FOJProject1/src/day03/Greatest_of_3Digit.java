package day03;

public class Greatest_of_3Digit {

    // Method to calculate the sum of three digits in a number
    public static int calculateDigitSum(int num) {
        int firstDigit = num / 100;
        int secondDigit = (num / 10) % 10;
        int thirdDigit = num % 10;
        int sum = firstDigit + secondDigit + thirdDigit;

        return sum;
    }
}
