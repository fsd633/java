package assignment3;

public class Demo6 {

    public static int isEven(int number) {
        // Check if the number is even, odd, or negative
        if (number <= 0)
            return -1;
        else if (number % 2 == 0)
            return 1;
        else
            return 0;
    }

    public static void main(String[] args) {
        // Test the isEven method with various numbers
        System.out.println(isEven(22));
        System.out.println(isEven(26));
        System.out.println(isEven(35));
        System.out.println(isEven(-5));
        System.out.println(isEven(0));
    }
}

