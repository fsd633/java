package day05;

public class Demo9 {
	
	public static boolean isPrime(int n){
	
		if ( n == 0 || n == 1)
              return false;
		
		for(int i = 2; i < n; i++){
			if ( n % i == 0)
				return false;
		}
		return true;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int N = 10;
		
		for ( int i = 1; i <= N ; i++){
			
			if(isPrime(i))
				System.out.println( i + "");
		}

	}

}

